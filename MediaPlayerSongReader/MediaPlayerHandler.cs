﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MediaPlayerSongReader.MediaPlayers;

namespace MediaPlayerSongReader
{
    class MediaPlayerHandler
    {
        public List<Process> GetAvailableMediaPlayers(List<string> mediaPlayerProcessNames )
        {
            Process[] procList = Process.GetProcesses( );

            List< Process > mediaPlayers = (from p in procList
                                            where mediaPlayerProcessNames.Contains(p.ProcessName.ToLower())
                                            select p).ToList( );

            return mediaPlayers;
        }
        public List<Process> GetAvailableMediaPlayers( string mediaPlayerProcessName )
        {
            return GetAvailableMediaPlayers( new List<string>() { mediaPlayerProcessName } );
        }

    }
}
