﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MediaPlayerSongReader.Exception;

namespace MediaPlayerSongReader.MediaPlayers
{
    class Spotify : MediaPlayer
    {
        private const string RegexpReplaceString = @"^spotify -|spotify";

        public Spotify(Process process) : base(process){}

        public new const string ProccessName = "spotify";
        public override string GetSongname( )
        {
            Process.Refresh();

            if( !Process.HasExited ){

                string songName = Regex.Replace( Process.MainWindowTitle, RegexpReplaceString, "", RegexOptions.IgnoreCase );
                songName = songName.Trim();
                return songName;
            }
            throw new MediaPlayerException("Process exited");
        }

        public override string Name
        {
            get { return "Spotify"; }
        }
    }
}
