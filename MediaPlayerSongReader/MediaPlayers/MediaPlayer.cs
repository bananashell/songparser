﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MediaPlayerSongReader.MediaPlayers
{
    abstract class MediaPlayer
    {
        protected MediaPlayer(Process process)
        {
            Process = process;
        }

        public Process Process { get; protected set; }

        public const string ProccessName = "";
        public abstract string GetSongname( );
        public abstract string Name { get; }

    }
}
