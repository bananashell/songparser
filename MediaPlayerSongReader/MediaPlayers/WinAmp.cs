﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MediaPlayerSongReader.Exception;

namespace MediaPlayerSongReader.MediaPlayers
{
    class WinAmp : MediaPlayer
    {
        private string lastSong = "";
        private string[] skipStrings = {"Notifier"};
        private const string RegexpReplaceString = @"^(\d*\. )| (- winamp)$| (- winamp \[\w*\])$|Winamp \d.\d*";

        public WinAmp( Process process ) : base( process ){}

        public new const string ProccessName = "winamp";
        public override string GetSongname( )
        {
            Process.Refresh();

            if (!Process.HasExited)
            {
                string songName = Regex.Replace(Process.MainWindowTitle, RegexpReplaceString, "", RegexOptions.IgnoreCase);
                if( !skipStrings.Contains(songName) )
                {
                    lastSong = songName;
                    return songName;    
                }
                
                return lastSong;
            }
            throw new MediaPlayerException("Process exited");
        }

        public override string Name
        {
            get { return "WinAmp"; }
        }
    }
}
