﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaPlayerSongReader.UserControls
{
    /// <summary>
    /// Interaction logic for OptionUserControl.xaml
    /// </summary>
    public partial class OptionUserControl : UserControl
    {


        public Window Parent { get; set; }

        public OptionUserControl()
        {
            InitializeComponent();

            //Fill combobox
            for (int i = 1; i <= 100; i++)
            {
                combShortenOutput.Items.Add(i);
            }

            //Set the options
            cbShortenOutput.IsChecked = Properties.Settings.Default.ShortenOutput;
            combShortenOutput.SelectedIndex = Properties.Settings.Default.ShortenOutputLength - 1;
            //combShortenOutput.SelectedItem = from object i in combShortenOutput.Items where i.ToString() == Properties.Settings.Default.ShortenOutputLength.ToString() select i;
            this.UpdateLayout();
        }


        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShortenOutput = cbShortenOutput.IsChecked.HasValue ? cbShortenOutput.IsChecked.Value : false;

            if (Properties.Settings.Default.ShortenOutput)
            {
                Properties.Settings.Default.ShortenOutputLength = Convert.ToInt32(combShortenOutput.SelectedItem.ToString());
            }


            if (Parent != null)
            {
                Parent.Close();
            }
        }
        
    }

}
