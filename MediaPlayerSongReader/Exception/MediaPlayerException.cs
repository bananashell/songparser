﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaPlayerSongReader.Exception
{
    class MediaPlayerException : System.Exception
    {
        public MediaPlayerException()
            : base()
        {

        }
        public MediaPlayerException( string message ) 
            : base( message )
        {
            
        }
    }
}
