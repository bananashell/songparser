﻿using System.IO;

namespace MediaPlayerSongReader
{
    static class FileWriter
    {
        public static void WriteToFile(string path, string textToWrite)
        {
            try
            {
                TextWriter tw = new StreamWriter(path, false);
                tw.WriteLine(textToWrite);
                tw.Close();
            }
            catch (System.Exception exception)
            {
                //ignore
            }
        }
    }
}
