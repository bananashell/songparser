﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaPlayerSongReader.Options
{
    class ShortenTextOption : Option
    {

        public static string PerformTextOption(string input)
        {
            if ( Properties.Settings.Default.ShortenOutput )
            {
                if (input.Length > Properties.Settings.Default.ShortenOutputLength)
                {
                    input = input.Substring(0, Properties.Settings.Default.ShortenOutputLength);
                    input += "…";
                }
            }

            return input;
        } 

    }
}
